package com.davrbk.simplemessenger.tools;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;

import com.davrbk.simplemessenger.activities.MainMessageActivity;

import java.util.Random;

/**
 * Created by david on 10/29/14.
 */
public class VirtualServerBroadcastReceiver extends BroadcastReceiver {

    private ServerListener _serverListener;

    final public static String MESSAGE = "message";

    @Override
    public void onReceive(Context context, Intent intent) {

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TAG");

        wl.acquire();


        final Bundle extras = intent.getExtras();

        final Handler h = MainMessageActivity.handler;
        _serverListener = MainMessageActivity.сallbackMap.get(h);

        //Отправляем сообщение с "сервера"
        h.post(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {

                    if(_serverListener != null) {
                        _serverListener.onMessageReceived(extras.getString(MESSAGE));
                    }
                }
            }
        });

        // Разблокируем поток.

        wl.release();

   }

    public interface ServerListener {

       void onMessageReceived(String message);

    }

    public void setServerListener(ServerListener serverListener) {
        _serverListener = serverListener;
    }

    public void removeServerListener() {
        _serverListener = null;
    }

    public void setSendingMessage(Context context) {

        AlarmManager am  = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent    = new Intent(context, VirtualServerBroadcastReceiver.class);
        intent.putExtra(MESSAGE, "This is an #awesome_app"); // Задаем параметр интента - наше сообщение

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);

        Random r         = new Random();
        int randomNumber = r.nextInt(15 - 3) + 3;

    // Устанавливаем интервал срабатывания в случайное количество (от 3 до 15) секунд.

        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * randomNumber, pi);

    }

    public void cancelSendingMessage(Context context) {

        Intent intent             = new Intent(context, VirtualServerBroadcastReceiver.class);
        PendingIntent sender      = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);

    }
}
