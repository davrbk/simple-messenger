package com.davrbk.simplemessenger.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.davrbk.simplemessenger.obj.Message;

import java.util.ArrayList;

/**
 * Created by david on 10/29/14.
 */
public class MyDBUtil {

    private SQLiteDatabase _database;

    private ExternalDbOpenHelper _dbOpenHelper;

    public MyDBUtil(Context context) {

    }


    //Метод добавления сообщения в список и записи в БД
    public void writeMessageIntoDB(String fromWho, String message, Context context, ArrayList<Message> messages) {

        Message m = new Message();
        m.setFromWho(fromWho);
        m.setMessage(message);
        messages.add(m);

        _dbOpenHelper = new ExternalDbOpenHelper(context, "SimpleMessengerDB.sqlite");
        _database = _dbOpenHelper.openDataBase();
        _database.execSQL("INSERT INTO messages (from_who, message) VALUES ('" + fromWho + "', '" + message + "')");
        _database.close();

    }

    //Метод заполнения списка из БД
    public ArrayList<Message> fillList(ArrayList<Message> messages, Context context) {

        _dbOpenHelper = new ExternalDbOpenHelper(context, "SimpleMessengerDB.sqlite");
        _database     = _dbOpenHelper.openDataBase();
        Cursor cursor = _database.rawQuery("SELECT * FROM messages", new String[] {});

        messages.clear();

        while(cursor.moveToNext()) {
            Message m = new Message(cursor);
            messages.add(m);
        }

        cursor.close();
        _database.close();

        return messages;
    }
}
