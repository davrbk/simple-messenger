package com.davrbk.simplemessenger.views;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.davrbk.simplemessenger.R;

/**
 * Created by david on 10/29/14.
 */
public class MainActivityView extends LinearLayout {

    private ListView _messageListView;

    private EditText _inputMessageEdit;

    private Button _sendMessageButton;

    private ViewListener _viewListener;

    public static interface ViewListener {

        void onSendButtonClicked(EditText text);

    }

    public MainActivityView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public synchronized void setViewListener(ViewListener viewListener) {
        _viewListener = viewListener;
    }

    public synchronized void removeViewListener() {
        _viewListener = null;
    }

    public synchronized ListView getListView() {
        return _messageListView;
    }
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        _messageListView   = (ListView) findViewById(R.id.messagesListView);
        _inputMessageEdit  = (EditText) findViewById(R.id.inputMessageEditText);
        _sendMessageButton = (Button) findViewById(R.id.sendMessageButton);


        //Устанавливаем высоту разделителя списка в ноль
        _messageListView.setDividerHeight(0);

        _inputMessageEdit.setHint("Введите сообщение");

        _sendMessageButton.setText("Отпр.");
        _sendMessageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                synchronized (this) {
                    if(_viewListener != null) {
                        _viewListener.onSendButtonClicked(_inputMessageEdit);
                    }
                }
            }
        });
    }
}
