package com.davrbk.simplemessenger.lists;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.davrbk.simplemessenger.R;
import com.davrbk.simplemessenger.obj.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 10/29/14.
 */
public class MessagesListAdapter extends ArrayAdapter {

    private ArrayList<Message> _messages;

    private Context _context;

    public MessagesListAdapter(Context context, int resource, ArrayList<Message> objects) {
        super(context, resource, objects);

        _messages = objects;
        _context  = context;

        refreshList();
    }

    @Override
    public int getCount() {
        return _messages.size();
    }

    @Override
    public Message getItem(int position) {
        return _messages.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null) {

            LayoutInflater li = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.item_list_messages, null);
        }

        TextView messageView = (TextView) v.findViewById(R.id.messageView);

        if(messageView != null) {

            //Если сообщение от меня то устанавливаем положение текста справа налево
            //иначе - слева направо
            String fromWho = getItem(position).getFromWho();
            if(fromWho.equals("me")) {
                messageView.setGravity(Gravity.END);
            } else {
                messageView.setGravity(Gravity.START);
            }


            messageView.setText(getItem(position).getMessage());

            //Выделяем текст который идет после "#"
            setTags(messageView, getItem(position).getMessage());
        }

        return v;
    }

    //Метод обновления списка
    public void refreshList() {
        super.notifyDataSetChanged();
    }

    //Метод выделения красным цветом текста который идет после "#"
    private void setTags(TextView pTextView, String pTagString) {

        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            Log.d("Hash", String.format("Clicked %s!", tag));
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {

                            // link color
                            ds.setColor(Color.parseColor("#FF0000"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }
}
