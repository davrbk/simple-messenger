package com.davrbk.simplemessenger.obj;

import android.database.Cursor;

/**
 * Created by david on 10/29/14.
 */
public class Message {

    private int id;

    private String fromWho;

    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFromWho() {
        return fromWho;
    }

    public void setFromWho(String fromWho) {
        this.fromWho = fromWho;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message() {}

    //Получение объекта Message из БД
    public Message(Cursor cursor) {

        id      = cursor.getInt(0);
        fromWho = cursor.getString(1);
        message = cursor.getString(2);
    }
}
