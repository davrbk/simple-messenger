package com.davrbk.simplemessenger.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.EditText;
import android.widget.ListView;

import com.davrbk.simplemessenger.R;
import com.davrbk.simplemessenger.lists.MessagesListAdapter;
import com.davrbk.simplemessenger.obj.Message;
import com.davrbk.simplemessenger.tools.VirtualServerBroadcastReceiver;
import com.davrbk.simplemessenger.utils.MyDBUtil;
import com.davrbk.simplemessenger.views.MainActivityView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class MainMessageActivity extends Activity implements MainActivityView.ViewListener,
        VirtualServerBroadcastReceiver.ServerListener {

    private MainActivityView _mainView;

    private MyDBUtil _dbUtil;

    private ListView _messagesView;

    private ArrayList<Message> _messagesList;

    private MessagesListAdapter _listAdapter;

    private VirtualServerBroadcastReceiver _virtualServer;

    public static Handler handler;

    public static Map<Handler, VirtualServerBroadcastReceiver.ServerListener> сallbackMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Инициализация View этого Activity
        _mainView = (MainActivityView) getLayoutInflater().inflate(R.layout.activity_main_message, null);

        //Устанавливаем слушатель View
        if(_mainView != null) {
            _mainView.setViewListener(this);
        }

        setContentView(_mainView);

        //Инициаизация утилиты помогающей работать с БД
        _dbUtil        = new MyDBUtil(this);

        //Инициализация ListView
        _messagesView  = _mainView.getListView();
        _messagesView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        _messagesView.setStackFromBottom(true);

        //Инициализация ArrayList c объектами Message
        _messagesList  = new ArrayList<Message>();

        //Инициализация адаптера
        _listAdapter   = new MessagesListAdapter(this, 0, _messagesList);

        //Инициализация "виртуального сервера"
        _virtualServer = new VirtualServerBroadcastReceiver();

        //Инициализация Map где мы храним слушатели и используем android.os.Handler в качестве ключа
        сallbackMap = new HashMap<Handler, VirtualServerBroadcastReceiver.ServerListener>();

        handler = new Handler();

    }

    @Override
    protected void onStart() {
        super.onStart();
        //Наполнение списка, установка адаптера ListView и автоматический скролл вниз списка
        _messagesList = _dbUtil.fillList(_messagesList, this);
        _messagesView.setAdapter(_listAdapter);
        scrollMyListViewToBottom();

        //Установка слушателя виртуального сервера, помещаем в Map этот слушатель
        //и ставим регулярную отправку сообщений
        if(_virtualServer != null) {
            _virtualServer.setServerListener(this);
            сallbackMap.put(handler, this);
            _virtualServer.setSendingMessage(MainMessageActivity.this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        //Отменяем регулярную отправку сообщений, удаляем слушатель (дабы избежать утечки)
        //и очищаем Map
        if(_virtualServer != null) {
            _virtualServer.cancelSendingMessage(MainMessageActivity.this);
            _virtualServer.removeServerListener();
            сallbackMap.clear();
        }
    }


    //Колбэк нажатия кнопки "отправить"
    @Override
    public void onSendButtonClicked(EditText text) {

        //Добавление в список и запись в БД сообщения, которое мы ввели
        _dbUtil.writeMessageIntoDB("me", text.getText().toString(), MainMessageActivity.this, _messagesList);


        //Убираем отправленное сообщение из поля EditText, обновляем список
        // и автоматически пролистываем в конец
        if(_messagesView != null) {

            text.setText("");
            text.setActivated(false);
            _listAdapter.refreshList();
            scrollMyListViewToBottom();
        }
    }

    //Колбэк принятого сообщения с виртуального сервера
    @Override
    public void onMessageReceived(String message) {

        _dbUtil.writeMessageIntoDB("not_me", message, MainMessageActivity.this, _messagesList);

        if(_messagesView != null) {

            _listAdapter.refreshList();
            scrollMyListViewToBottom();
        }
    }

    //Метод атоматического пролистывания списка вниз
    private void scrollMyListViewToBottom() {
        _messagesView.post(new Runnable() {
            @Override
            public void run() {
                _messagesView.setSelection(_listAdapter.getCount() - 1);
            }
        });
    }
}
